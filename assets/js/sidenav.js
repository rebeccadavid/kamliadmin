$('.side-nav-toggle').click(function() {
    if ($('.side-nav-toggle i').hasClass('fa-reorder')){
        $('.side-nav-toggle i').removeClass('fa-reorder');
        $('.side-nav-toggle i').addClass('fa-close');
        $('.mobile-sidenav').css('left','0');
    } else {
        $('.side-nav-toggle i').addClass('fa-reorder');
        $('.side-nav-toggle i').removeClass('fa-close');
        $('.mobile-sidenav').css('left','-100%');
      }
});
$(".phm-close").click(function() {
  $('.mobile-sidenav').css('left','-100%');
  $('.side-nav-toggle i').removeClass('fa-close');
  $('.side-nav-toggle i').addClass('fa-reorder');
});
